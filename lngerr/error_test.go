package lngerr

//import (
//	"context"
//	"testing"
//
//	"gitlab.com/adhocguru/fcp/apis/gen/order/v1/order"
//	"google.golang.org/genproto/googleapis/rpc/errdetails"
//	"google.golang.org/grpc/codes"
//)

//func TestToGRPC(t *testing.T) {
//	var ErrInvalidOrderStatus = New("INVALID_ORDER_STATUS", "invalid order status", "order has invalid status(%v)").WithCode(codes.FailedPrecondition)
//
//	ErrInvalidOrderStatus = ErrInvalidOrderStatus.WithParams(order.Order_DONE).WithInfo("inactive order status", "status", order.Order_DONE)
//	ctx := context.Background()
//	grpcErr := ErrInvalidOrderStatus.ToGRPC(ctx)
//
//	details := map[string][]string{}
//	details["code"] = []string{grpcErr.Code().String()}
//	for _, detail := range grpcErr.Details() {
//		switch t := detail.(type) {
//		case *errdetails.ErrorInfo:
//			details["domain"] = append(details["domain"], t.GetDomain())
//			details["reason"] = append(details["reason"], t.GetReason())
//			for k, v := range t.GetMetadata() {
//				details[k] = append(details[k], v)
//			}
//		case *errdetails.LocalizedMessage:
//			details["locale"] = append(details["locale"], t.GetLocale())
//			details["localized_message"] = append(details["localized_message"], t.GetMessage())
//		case *errdetails.PreconditionFailure:
//			for _, violation := range t.GetViolations() {
//				details[violation.Type] = append(details[violation.Type], violation.GetDescription(), violation.GetSubject())
//			}
//		case *errdetails.BadRequest:
//			for _, violation := range t.GetFieldViolations() {
//				details[violation.GetField()] = append(details[violation.GetField()], violation.GetDescription())
//			}
//		}
//	}
//
//	t.Log(grpcErr)
//	t.Log(details)
//}

package utils

import (
	"testing"
)

type mytype struct {
	next *mytype
	prev *mytype
}

type tobj struct {

	FSliceStruct      []*tobj
	FSlice            []string
	FPoint            *int
	FMap              map[string]int
	FArray            [4]int
	finternal         string
	XXX_NoUnkeyed     struct{}
	XXX_unrecognized  []byte
	XXX_sizecache     int32
}

func TestPrintPoint(t *testing.T) {
	var v1 = new(mytype)
	var v2 = new(mytype)

	v1.prev = nil
	v1.next = v2

	v2.prev = v1
	v2.next = nil

	Display("v1", v1, "v2", v2)
}

func TestPrintString(t *testing.T) {
	str := GetDisplayString("v1", 1, "v2", 2)
	println(str)
}

func TestToJson(t *testing.T) {

	obj := &tobj{
		FSliceStruct:     []*tobj{ &tobj{}, &tobj{}, &tobj{}, &tobj{}, &tobj{}, &tobj{}, &tobj{}, &tobj{}, &tobj{}, &tobj{}, &tobj{}, &tobj{} },
		FSlice:           []string{"s1", "s2", "s3"},
		FPoint:           new(int),
		FMap:             map[string]int{"bob": 5, "helen": 25},
		FArray:           [4]int{1, 2, 3, 4},
		finternal:        "internal field",
	}

	println(ToJsonPretty( obj ))
	println(ToJson( obj ))
	println(ToJson( obj, false ))
	println(ToJson( obj, true ))

	println(ToString( obj ))
}
package utils

import (
	"context"

	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
)

func SetErrorTag(ctx *context.Context, err *error) {
	if ctx != nil && err != nil && *err != nil {
		if sp := opentracing.SpanFromContext(*ctx); sp != nil {
			ext.Error.Set(sp, true)
		}
	}
}

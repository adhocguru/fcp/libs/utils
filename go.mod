module gitlab.com/adhocguru/fcp/libs/utils

go 1.16

require (
	github.com/asaskevich/govalidator v0.0.0-20210307081110-f21760c49a8d // indirect
	github.com/go-ozzo/ozzo-validation v3.6.0+incompatible
	github.com/google/uuid v1.3.0
	github.com/oklog/ulid v1.3.1
	github.com/opentracing/opentracing-go v1.2.0
	github.com/stretchr/testify v1.7.0
	google.golang.org/genproto v0.0.0-20211005153810-c76a74d43a8e
	google.golang.org/grpc v1.41.0
)

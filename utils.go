package utils

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"os"
	"path/filepath"
	"reflect"
	"runtime"
	"strings"
	"time"
)

// GetGOPATHs returns all paths in GOPATH variable.
func GetGOPATHs() []string {
	gopath := os.Getenv("GOPATH")
	if gopath == "" && strings.Compare(runtime.Version(), "go1.8") >= 0 {
		gopath = defaultGOPATH()
	}
	return filepath.SplitList(gopath)
}

func defaultGOPATH() string {
	env := "HOME"
	if runtime.GOOS == "windows" {
		env = "USERPROFILE"
	} else if runtime.GOOS == "plan9" {
		env = "home"
	}
	if home := os.Getenv(env); home != "" {
		return filepath.Join(home, "go")
	}
	return ""
}

func HideFields(obj interface{}, fieldPrefixes ...string) {
	defer func() { recover() }()
	v := reflect.ValueOf(obj)
	hideFields(v, fieldPrefixes...)
}

func hideFields(v reflect.Value, fieldPrefixes ...string) {
	if v.Kind() == reflect.Ptr {
		v = v.Elem()
	}
	if v.Kind() != reflect.Struct {
		return
	}

	t := v.Type()

	for i := 0; i < t.NumField(); i++ {
		name := t.Field(i).Name
		value := v.FieldByName(name)

		if value.Kind() == reflect.Ptr {
			value = value.Elem()
		}
		if value.Kind() == reflect.Struct {
			hideFields(value, fieldPrefixes...)
			continue
		}

		nameLower := strings.ToLower(name)
		hasPrefixes := false
		for _, prefix := range fieldPrefixes {
			if strings.Contains(nameLower, strings.ToLower(prefix)) {
				hasPrefixes = true
				break
			}
		}

		if hasPrefixes && value.Kind() == reflect.String {
			if value.CanSet() && value.CanAddr() {
				value.SetString("*****")
			}
		}
	}
}

func GetMD5Hash(text string) string {
	hasher := md5.New()
	hasher.Write([]byte(text))
	return hex.EncodeToString(hasher.Sum(nil))
}

func EscapeString(value string) string {
	replaceList := []struct {
		old string
		new string
	}{
		{"\\", "\\\\"},
		{"'", `''`},
		{"\\0", "\\\\0"},
		{"\n", "\\n"},
		{"\r", "\\r"},
		{`"`, `\"`},
		{"\x1a", "\\Z"},
	}

	for _, replace := range replaceList {
		value = strings.Replace(value, replace.old, replace.new, -1)
	}

	return value
}

func GetProtobufFieldName(obj interface{}, fieldName string) string {
	val := reflect.Indirect(reflect.ValueOf(obj))
	if sf, ok := val.Type().FieldByName(fieldName); ok {
		tagValue := sf.Tag.Get("protobuf")
		if len(tagValue) == 0 { return ""}
		tagValue = strings.Replace(tagValue, " ", "", -1)
		paramName := "name="

		for _, paramValue := range strings.Split(tagValue, ",") {
			if strings.HasPrefix(paramValue, paramName) {
				return strings.TrimPrefix(paramValue, paramName)
			}
		}
	}
	return ""
}

func TimeInTZ(t time.Time, tz string) (tTZ time.Time, err error) {
	loc, err := time.LoadLocation(tz)
	if err != nil {
		return time.Time{}, err
	}
	return t.In(loc), nil
}

func GetFunctionName(i interface{}) string {
	f := runtime.FuncForPC(reflect.ValueOf(i).Pointer())
	if f == nil{
		return ""
	}
	file, line := f.FileLine(f.Entry())
	return fmt.Sprintf("%s:%d %s", file, line, f.Name())
}

func LogStack() string {
	stack := []string{}
	i := 0
	for {
		pc, file, line, ok := runtime.Caller(i)
		if !ok {
			break
		}

		stack = append(stack, fmt.Sprintf("%s:%d %s <-", file, line, runtime.FuncForPC(pc).Name()))
		i++
	}
	return strings.Join(stack, " *** ")
}